
import { Injectable } from '@angular/core';
import { v4 as uuidv4} from 'uuid';

@Injectable()
export class UuidService{

    private uuidUnique = 0;
    private invalid = /^[A-Za-z]+[\w\-\:\.]*$/;

    public getUuidUnique(): number {
      return this.uuidUnique;
    }

    public createdUniqueUuidWithPrefix(prefix: string): string {
      if (!prefix || !this.invalid.test(prefix)){
        throw Error('Prefix is required');
      }

      const uuid = this.createdUniqueUuid();
      this.uuidUnique++;
      return `${prefix}-${uuid}`;
    }

    private createdUniqueUuid(): string {
      return uuidv4();
    }

}
