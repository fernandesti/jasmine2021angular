import { TestBed } from "@angular/core/testing";
import { UuidService } from "./uuid.service"

describe(UuidService.name, () => {

  let uuidService: UuidService;

  beforeEach(() => {
    uuidService = new UuidService();
  });

  it(`#${UuidService.prototype.createdUniqueUuidWithPrefix.name} deve retornar um prefixo`, () => {
    const sufix = uuidService.createdUniqueUuidWithPrefix('app');
    //expect(sufix).toContain('app-');
    expect(sufix.startsWith('app-')).toBeTrue();
  });

  it(`#${UuidService.prototype.createdUniqueUuidWithPrefix.name} deve retornar ids diferentes`, () => {
    const ids = new Set();
    for (let i = 0; i < 50; i++) {
      ids.add(uuidService.createdUniqueUuidWithPrefix('app'));
    }
    expect(ids.size).toBe(50);
  });

  it(`#${UuidService.prototype.createdUniqueUuidWithPrefix.name} deve retornar erro caso nao tenha prefixo ou seja invalido`, () => {
    const invalidos = [null, undefined, '', '1'];
    invalidos.forEach(invalido => {
      expect(() => uuidService.createdUniqueUuidWithPrefix(invalido))
                  .withContext(`contexto atual: ${invalido}`)
                  .toThrow();
    });
  });

  it(`#${UuidService.prototype.getUuidUnique.name} deve estar incrementando uuid`, () => {
    uuidService.createdUniqueUuidWithPrefix('app');
    uuidService.createdUniqueUuidWithPrefix('app');
    expect(uuidService.getUuidUnique()).toBe(2);
  });

});
