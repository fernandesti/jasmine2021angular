import { Component, Input, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

import { faThumbsUp } from '@fortawesome/free-solid-svg-icons';
import { UuidService } from 'src/app/services/Uuid-service/uuid.service';

@Component({
  selector: 'app-like-widget',
  templateUrl: './like-widget.component.html',
  styleUrls: ['like-widget.component.scss']
})
export class LikeWidgetComponent implements OnInit {

  @Output() public liked = new EventEmitter<void>();
  @Input() public likes = 0;
  @Input() public id = null;
  public icons = {
    faThumbsUp
  };

  constructor(private uuidService: UuidService){}

  ngOnInit(): void {
    if (!this.id) {
      this.id = this.uuidService.createdUniqueUuidWithPrefix('like-widget');
    }
  }

  public like(): void {
    this.liked.emit();
  }

}
