import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { UuidService } from '../services/Uuid-service/uuid.service';
import { LikeWidgetComponent } from './LikeWidget/like-widget.component';

const components = [
  LikeWidgetComponent,
];

@NgModule({
  declarations: [
    ...components
  ],
  imports: [
    CommonModule,
    FontAwesomeModule
  ],
  exports: [
    ...components
  ],
  providers: [
    UuidService
  ]

})
export class ComponentModule {}
